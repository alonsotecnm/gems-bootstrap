(function(){

    angular.module('constants', [])

    .constant('API_ENDPOINT', {
        url: 'http://localhost:3000'
    })

    .constant('ENDPOINTS', {
        newGem: '/api/igems',
        getAllGems: '/api/gems',
        getGemById: '/api/gem',
        deleteGem: '/api/dgems',
        updateGem: '/api/ugems',
        uploadImageToGem: '/api/uimages'
    })

})();